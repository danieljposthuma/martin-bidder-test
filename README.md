# agency-test-tools

A collection of tools used to test agency projects

## Tools

### Augmentor-Stream
Sends requests to the augmentor to test if it is properly augmenting requests. Displays expected and actual augmented counts at the end of the run.

#### Environment
* `TARGET_URL` - Lets you change the augmentor to test. Default: http://localhost:3000/
* `MATCHES_FILE` - A CSV of matches to look for. Path relative to where you are running from. Default: ./data/augmentor-matches.csv
* `MISSES_FILE` - A CSV of misses to mix the matches into. Path relative to where you are running from. Default: ./data/augmentor-missed.csv
* `STREAM` - If true, sends a continuous stream of requests instead of checking the number augmented. Useful for testing memory leaks or QPS. Default: false
* `QPS` - How many queries to send per second when streaming. Maximum 1000, default 100.
* `DATA_TYPE` - 'json' or 'protobuf', default 'protobuf'

### Bidder-Stream
Sends requests to the bidder to test if it is properly bidding.

#### Environment
* `TARGET_URL` - Lets you change the augmentor to test. Default: http://localhost:3000/
* `BIDS_FILE` - A CSV of matches to look for. Path relative to the bid-stream.js file. Default: ./../data/bid-requests.json
* `STREAM` - If true, sends a continuous stream of requests instead of checking the number augmented. Useful for testing memory leaks or QPS. Default: false
* `QPS` - How many queries to send per second when streaming. Maximum 1000, default 100.
* `DATA_TYPE` - 'json' or 'protobuf', default 'protobuf'