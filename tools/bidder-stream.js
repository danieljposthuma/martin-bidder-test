/**
 * This tool tests that the bidder is bidding correctly
 */

// Requires
// const assert = require('assert');
const asynclib = require('async');
const csv = require('fast-csv');
const request = require('request');
const DATA_TYPE = process.env.DATA_TYPE || 'json'

// Constants/Globals
const TARGET_URL = process.env.TARGET_URL || 'http://localhost:3000/';
const BIDS_FILE = process.env.BIDS_FILE || './../data/martin_bidder_bid_requests.json'
let bids = require(BIDS_FILE);
// How often to send requests while streaming
// Set interval won't allow above 1k
const QPS = parseInt(process.env.QPS) || 100;



let BidList = [];

bids.forEach((bid) => {
  BidList.push(bid);
});


// Send once
BidList.forEach((bid) => {
  sendRequest(bid, (err) => {

    if(err) {
      console.log("Final error:", err);
    } else { 
      console.log(bid);
    }

  }); // sendRequests   
});


// Sends a continuous stream of bids to the augmentor
function streamRequests(bids, callback) {

  let interval = setInterval(() => {

    let bid = bids.shift();
    sendRequest(bid, (err) => {
      // No harm in shifting the element if an error did occur
      bids.push(bid);
      
      if(err) {
        // Error occurred, bail out
        clearInterval(interval);
        return callback(err);
      }

    }); // sendRequest

  }, 1000 / QPS); // setInterval

}

// Sends a single request
function sendRequest(auction, callback) {

  let formatted_bid = auction;
  let content_type;

    formatted_bid = auction
    content_type = 'application/json';

  let requestObject = {
    url: TARGET_URL,
    method: 'POST',
    headers: {
      'Content-Type': content_type
    },
    body: JSON.stringify(formatted_bid),
    encoding: null
  };

  request(requestObject, (err, response, body) => {

    if(response && response.statusCode == 200) {
      console.log("Response received")
      // body = protos.bid.AugmentorResponse.decode(body);
      // console.log("Bid on:", bid.bidRequest.site.page);
    } else {
      // console.log("Passed on:", bid.bidRequest.site.page);
    }
    
    return callback(err);
  });

}

