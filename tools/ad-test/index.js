
const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 8080;
const axios = require('axios');
const fs = require('fs');

const app = express();
app.use(express.static('public'));

let index = path.join(__dirname, '/public/index.html');
let js = path.join(__dirname, '/public/script.js');

app.get('/test', (req, res) => {

    const creativeId = "5c4b78be7e85ed2389eddde9"
    const bidId = 0;

    axios.get(`http://localhost:3000/jstag?tagId=${creativeId}&bidId=${bidId}`)
    .then(response => {
        let tag = response.data;

        //fs.writeFileSync(js, tag, 'utf8', (err) => {
            //console.log(err);
        //});
        
        setTimeout(() => {
        
            return res.sendFile(index);
        }, 1000);
    })
    .catch((error) => {
        console.log(error);
    });
});


app.listen(PORT, (err) => {
    if(err) console.log(err);
    console.log(`The server is running on Port: ${PORT}`);
});
